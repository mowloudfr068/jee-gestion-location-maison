# Gestion Immobilier

Ce projet de gestion immobilière est développé en utilisant Java, JSP et MySQL. Il permet de gérer les logements, les visiteurs et les agences immobilières.

## Étapes pour utiliser le projet

1. Assurez-vous d'avoir installé Java et MySQL sur votre machine.
2. Importez le projet dans votre environnement de développement (par exemple, Eclipse).
3. Configurez la base de données en créant une base de données MySQL et en important le script de création de la base de données (fichier .sql fourni avec le projet).
4. Ouvrez le fichier `db.properties` dans le répertoire `src/main/resources` et modifiez les informations de connexion à la base de données selon votre configuration.
5. Démarrez votre serveur d'applications (par exemple, Tomcat).
6. Accédez au lien suivant dans votre navigateur : [http://localhost:8080/JeeGestionImmobilier/login.jsp](http://localhost:8080/JeeGestionImmobilier/login.jsp)
7. Utilisez les informations de connexion suivantes pour vous connecter en tant qu'administrateur :
   - Email : admin@gmail.com
   - Mot de passe : admin
8. Une fois connecté en tant qu'administrateur, vous pouvez gérer les logements, les visiteurs et les agences immobilières en utilisant les différentes fonctionnalités disponibles.
9. Pour accéder à la page des visiteurs, utilisez le lien suivant : [http://localhost:8080/JeeGestionImmobilier/index.jsp](http://localhost:8080/JeeGestionImmobilier/index.jsp)

## Configuration de la base de données

Assurez-vous de configurer correctement la base de données . Vous devez créer une base de données MySQL et importer le script de création de la base de données fourni avec le projet.

