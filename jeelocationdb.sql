-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 17 juil. 2023 à 12:56
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `jeelocationdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `communes`
--

CREATE TABLE `communes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `distance_agence` int(11) NOT NULL,
  `nbr_habitant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `communes`
--

INSERT INTO `communes` (`id`, `libelle`, `distance_agence`, `nbr_habitant`) VALUES
(1, 'dar naim', 60, 2000),
(2, 'ksar', 12, 1000),
(3, 'nema', 500, 200),
(4, 'communetest', 10, 5000),
(5, 'arafat', 50, 10000),
(8, 'ndb', 400, 2000);

-- --------------------------------------------------------

--
-- Structure de la table `logements`
--

CREATE TABLE `logements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `superficie` double(8,2) NOT NULL,
  `loyer` double(8,2) NOT NULL,
  `id_quartiers` bigint(20) UNSIGNED NOT NULL,
  `id_type_logements` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=>occuper 0 disponible',
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `logements`
--

INSERT INTO `logements` (`id`, `libelle`, `address`, `superficie`, `loyer`, `id_quartiers`, `id_type_logements`, `img`, `status`, `description`) VALUES
(4, 'dar mowloud', 'ck4', 120.00, 45000.00, 4, 2, 'm8.jpg', 0, 'dar zeyne vihe 4 byout w hol w salon w mbar lgeddam tenkre'),
(6, 'dar ahmed', 'ck20', 400.00, 30000.00, 6, 2, 'm3.jpg', 0, 'une belle maison dans un emplacement strategique '),
(7, 'etage mohamed', 'ck10', 180.00, 70000.00, 3, 3, 'm6.jpg', 1, 'dar zeyne vihe 4 byout w hol w salon w mbar lgeddam tenkre'),
(8, 'dar kasoum', 'ck4', 400.00, 50000.00, 2, 2, 'm7.jpg', 0, '										dar zeine vihe 3 beyt w vihe hol w salon w espace lgedam\r\n										\r\n										\r\n										\r\n										'),
(11, 'dar mouna', 'hk20', 500.00, 70000.00, 11, 2, 'm9.jpg', 0, 'une belle maison contient 5 chambre et salon et 2 toilette et couisine et tente');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `nom_envoyeur` varchar(100) DEFAULT NULL,
  `titre` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `vue` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`id`, `nom_envoyeur`, `titre`, `description`, `telephone`, `created_at`, `vue`) VALUES
(1, 'med', 'maison à vendre', '12345678', '                    3nddi dar nbi3ha', '2023-07-15 23:04:58', 1),
(2, 'med', 'maison à vendre', '                    3ndi dar tnba3 ndor samsar lebas bih', '12345678', '2023-07-15 23:34:32', 1),
(3, 'mowloud', 'maison à vendre', '                   je une maison je veux le vendre esque vous peux deposer moi sur vortre site ', '49874807', '2023-07-16 00:02:13', 0),
(4, 'med', 'dar tenkre', '                  mankoum  7asin b dar tenkre v arafat  ', '49874807', '2023-07-16 01:08:10', 0),
(5, 'med', 'vendre une maison', '                    ndor dar zeyne v tvz', '49874807', '2023-07-16 09:30:13', 1);

-- --------------------------------------------------------

--
-- Structure de la table `quartiers`
--

CREATE TABLE `quartiers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `id_communes` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `quartiers`
--

INSERT INTO `quartiers` (`id`, `libelle`, `id_communes`) VALUES
(2, 'poto61', 5),
(3, 'batigue 10', 2),
(4, 'souk mekka', 2),
(6, 'welbadou', 1),
(9, 'welbadou', 1),
(10, 'char3 police', 5),
(11, 'mechrou3', 2);

-- --------------------------------------------------------

--
-- Structure de la table `type_logements`
--

CREATE TABLE `type_logements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_logement` varchar(255) NOT NULL,
  `charge_forfetaire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `type_logements`
--

INSERT INTO `type_logements` (`id`, `type_logement`, `charge_forfetaire`) VALUES
(2, 'etage', 5000),
(3, 'maison', 5000);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(1) NOT NULL COMMENT 'admin=>1 0=>user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `email`, `password`, `is_admin`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `communes`
--
ALTER TABLE `communes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `logements`
--
ALTER TABLE `logements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logements_id_quartiers_foreign` (`id_quartiers`),
  ADD KEY `logements_id_type_logements_foreign` (`id_type_logements`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `quartiers`
--
ALTER TABLE `quartiers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quartiers_id_communes_foreign` (`id_communes`);

--
-- Index pour la table `type_logements`
--
ALTER TABLE `type_logements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `communes`
--
ALTER TABLE `communes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `logements`
--
ALTER TABLE `logements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `quartiers`
--
ALTER TABLE `quartiers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `type_logements`
--
ALTER TABLE `type_logements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `logements`
--
ALTER TABLE `logements`
  ADD CONSTRAINT `logements_id_quartiers_foreign` FOREIGN KEY (`id_quartiers`) REFERENCES `quartiers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `logements_id_type_logements_foreign` FOREIGN KEY (`id_type_logements`) REFERENCES `type_logements` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `quartiers`
--
ALTER TABLE `quartiers`
  ADD CONSTRAINT `quartiers_id_communes_foreign` FOREIGN KEY (`id_communes`) REFERENCES `communes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
