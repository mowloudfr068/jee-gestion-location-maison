package com.location.maison.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

/**
 * Servlet implementation class DeleteCommuneServlet
 */
@WebServlet("/DeleteCommuneServlet")
public class DeleteCommuneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCommuneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get the id_candidat parameter from the request
	    int id_commune = Integer.parseInt(request.getParameter("id_commune"));
	    
	    
		
	    
	    // Prepare the SQL statement to delete the candidate
	   
	    
		try {
			 Connection conn = DBConnection.getConnection();
			PreparedStatement stmt;
			String sql = "DELETE FROM communes WHERE id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id_commune);
			// Execute the SQL statement
		    int rowsDeleted = stmt.executeUpdate();
		 // Close the database connection
		    conn.close();
		    // Redirect back to the candidates page
		    response.sendRedirect("communes.jsp");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	    
	    
	    
	    
	}

}
