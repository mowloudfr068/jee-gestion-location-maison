package com.location.maison.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

@WebServlet("/DeleteLogementServlet")
public class DeleteLogementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idLogement = Integer.parseInt(request.getParameter("id_logement"));

        try (Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection
                        .prepareStatement("DELETE FROM logements WHERE id = ?")) {
            preparedStatement.setInt(1, idLogement);

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                // Redirection vers une page de succès ou affichage d'un message de succès
                response.sendRedirect("logements.jsp");
            } else {
                // Redirection vers une page d'erreur ou affichage d'un message d'erreur
                response.sendRedirect("error.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Redirection vers une page d'erreur ou affichage d'un message d'erreur
            response.sendRedirect("error.jsp");
        }
    }
}
