package com.location.maison.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.location.maison.DBConnection;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public LoginServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        RequestDispatcher requestDispatcher = null;
        if (email == null || email.equals("")) {
            request.setAttribute("status", "InvalidUsername");
            requestDispatcher = request.getRequestDispatcher("login.jsp");
            requestDispatcher.forward(request, response);
        }
        if (password == null || password.equals("")) {
            request.setAttribute("status", "InvalidPassword");
            requestDispatcher = request.getRequestDispatcher("login.jsp");
            requestDispatcher.forward(request, response);
        }

        try {
            Connection con = DBConnection.getConnection();
            PreparedStatement preparedStatement = con
                    .prepareStatement("select * from users where email = ? and password = ?");

            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                session.setAttribute("name", rs.getString("nom"));
                requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
                
            } else {
                request.setAttribute("status", "failed");
                requestDispatcher = request.getRequestDispatcher("login.jsp");
                
            }

            rs.close();
            preparedStatement.close();
            con.close();
            requestDispatcher.forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
