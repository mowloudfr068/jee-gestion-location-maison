package com.location.maison.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.location.maison.DBConnection;

/**
 * Servlet implementation class SaveMessageServlet
 */
@WebServlet("/SaveMessageServlet")
public class SaveMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveMessageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		HttpSession session = request.getSession();
		RequestDispatcher requestDispatcher = null;
		if (request.getParameter("nom").equals("") || request.getParameter("titre").equals("") || request.getParameter("telephone").equals("") || request.getParameter("description").equals("")) {
			request.setAttribute("status", "required");
			requestDispatcher = request.getRequestDispatcher("contact.jsp");
            requestDispatcher.forward(request, response);
		} else {
		String nom = request.getParameter("nom");
		String titre = request.getParameter("titre");
		String telephone = request.getParameter("telephone");
		String description = request.getParameter("description");
		try{  
	        Connection conn=DBConnection.getConnection();  
	        PreparedStatement ps=conn.prepareStatement(  
	"INSERT INTO `messages`(`nom_envoyeur`,`titre`,`description`,`telephone`) VALUES(?,?,?,?)");  
	        ps.setString(1,nom);  
	        ps.setString(2,titre);  
	        ps.setString(3,description); 
	        ps.setString(4,telephone); 
	        int status=ps.executeUpdate();
	        if(status==1) {
	        	session.setAttribute("alert_add","la message et envoiyer avec succes");
				response.sendRedirect("index.jsp");
			}
			else {
				printWriter.println("not added");
			}
	    }catch(Exception e){System.out.println(e);}
	}
	}
}
