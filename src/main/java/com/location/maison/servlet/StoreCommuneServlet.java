package com.location.maison.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.location.maison.DBConnection;
import com.mysql.cj.Session;

/**
 * Servlet implementation class StoreCommuneServlet
 */
@WebServlet("/StoreCommuneServlet")
public class StoreCommuneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public StoreCommuneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		HttpSession session = request.getSession();
		RequestDispatcher requestDispatcher = null;
		if (request.getParameter("libelle").equals("") || request.getParameter("distance_agence").equals("") || request.getParameter("nombre_habitant").equals("")) {
			request.setAttribute("status", "required");
			requestDispatcher = request.getRequestDispatcher("createCommune.jsp");
            requestDispatcher.forward(request, response);
		} else {
		String libelle = request.getParameter("libelle");
		int distance_agence = Integer.parseInt(request.getParameter("distance_agence"));
		int nombre_habitant = Integer.parseInt(request.getParameter("nombre_habitant"));
		
		try{  
	        Connection conn=DBConnection.getConnection();  
	        PreparedStatement ps=conn.prepareStatement(  
	"INSERT INTO `communes`(`libelle`, `distance_agence`, `nbr_habitant`) VALUES(?,?,?)");  
	        ps.setString(1,libelle);  
	        ps.setInt(2,distance_agence);  
	        ps.setInt(3,nombre_habitant);   
	        int status=ps.executeUpdate();
	        if(status==1) {
	        	session.setAttribute("alert_add","la commune et ajouter avec succes");
				response.sendRedirect("communes.jsp");
			}
			else {
				printWriter.println("not added");
			}
	    }catch(Exception e){System.out.println(e);}
		}
		
	}

}
