package com.location.maison.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import com.location.maison.DBConnection;

/**
 * Servlet implementation class StoreLogementServlet
 */
@WebServlet("/StoreLogementServlet")
@MultipartConfig
public class StoreLogementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StoreLogementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Récupérer les données soumises par le formulaire
    	PrintWriter out = response.getWriter();
        String libelle = request.getParameter("libelle");
        String address = request.getParameter("address");
        int superficie = Integer.parseInt(request.getParameter("superficie"));
        int loyer = Integer.parseInt(request.getParameter("loyer"));
        int idQuartier = Integer.parseInt(request.getParameter("quartier"));
        int idTypeLogement = Integer.parseInt(request.getParameter("typeLogement"));
        Part file = request.getPart("img"); 
        int status = Integer.parseInt(request.getParameter("status"));
        String description = request.getParameter("description");
        String imageFileName=file.getSubmittedFileName();  // get selected image file name
        String uploadPath="C:/Users/Mohamed Mowloud/eclipse-workspace/JeeGestionImmobilier/src/main/webapp/images/"+imageFileName;  // upload path where we have to upload our actual image
     // Uploading our selected image into the images folder
		
     		try
     		{
     		
     		FileOutputStream fos=new FileOutputStream(uploadPath);
     		InputStream is=file.getInputStream();
     		
     		byte[] data=new byte[is.available()];
     		is.read(data);
     		fos.write(data);
     		fos.close();
     		
     		}
     		
     		catch(Exception e)
     		{
     			e.printStackTrace();
     		}
        // Enregistrer les données dans la base de données
        try (Connection connection = DBConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "INSERT INTO logements (libelle, address, superficie, loyer, id_quartiers, id_type_logements,img,status,description) " +
                             "VALUES (?, ?, ?, ?, ?, ?, ?,?,?)")) {
            preparedStatement.setString(1, libelle);
            preparedStatement.setString(2, address);
            preparedStatement.setInt(3, superficie);
            preparedStatement.setInt(4, loyer);
            preparedStatement.setInt(5, idQuartier);
            preparedStatement.setInt(6, idTypeLogement);
            preparedStatement.setString(7, imageFileName);
            preparedStatement.setInt(8, status);
            preparedStatement.setString(9, description);

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
            	// Actualisation du contenu du dossier images
                String cheminDossierImages = "C:/Users/Mohamed Mowloud/eclipse-workspace/JeeGestionImmobilier/src/main/webapp/images/";
                File dossierImages = new File(cheminDossierImages);

                if (dossierImages.isDirectory()) {
                    File[] fichiers = dossierImages.listFiles();
                    for (File fichier : fichiers) {
                        fichier.lastModified(); // Cette action actualise les métadonnées du fichier
                    }
                }
                // Redirection vers une page de succès ou affichage d'un message de succès
            	response.sendRedirect("logements.jsp");
            } else {
                // Redirection vers une page d'erreur ou affichage d'un message d'erreur
                out.println("erreur qui ce produit!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Redirection vers une page d'erreur ou affichage d'un message d'erreur
            out.println("erreur de connexion !");
        }
    }


}
