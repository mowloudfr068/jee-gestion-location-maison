package com.location.maison.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

/**
 * Servlet implementation class StoreQuartierServlet
 */
@WebServlet("/StoreQuartierServlet")
public class StoreQuartierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public StoreQuartierServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		String libelle = request.getParameter("libelle");
		int commune_id = Integer.parseInt(request.getParameter("commune_id"));
		try{  
	        Connection conn=DBConnection.getConnection();  
	        PreparedStatement ps=conn.prepareStatement(  
	"INSERT INTO `quartiers`(`libelle`, `id_communes`) VALUES(?,?)");  
	        ps.setString(1,libelle);  
	        ps.setInt(2,commune_id);     
	        int status=ps.executeUpdate();
	        if(status==1) {
				response.sendRedirect("quartiers.jsp");
			}
			else {
				printWriter.println("not added");
			}
	    }catch(Exception e){System.out.println(e);} 
	}

}
