package com.location.maison.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

/**
 * Servlet implementation class StoreTypeLogementServlet
 */
@WebServlet("/StoreTypeLogementServlet")
public class StoreTypeLogementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StoreTypeLogementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter =response.getWriter();
		String libelle = request.getParameter("libelle");
		int charge_forfetaire = Integer.parseInt(request.getParameter("charge_forfetaire"));
		try{  
	        Connection conn=DBConnection.getConnection();  
	        PreparedStatement ps=conn.prepareStatement(  
	"INSERT INTO `type_logements`(`type_logement`, `charge_forfetaire`) VALUES(?,?)");  
	        ps.setString(1,libelle);  
	        ps.setInt(2,charge_forfetaire);     
	        int status=ps.executeUpdate();
	        if(status==1) {
				response.sendRedirect("type_logements.jsp");
			}
			else {
				printWriter.println("not added");
			}
	    }catch(Exception e){System.out.println(e);}
	}

}
