package com.location.maison.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

/**
 * Servlet implementation class UpdateCommuneServlet
 */
@WebServlet("/UpdateCommuneServlet")
public class UpdateCommuneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCommuneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Retrieve the submitted form data
		PrintWriter printWriter = response.getWriter();
		int id = Integer.parseInt(request.getParameter("id_commune"));
		String libelle = request.getParameter("libelle");
		int distance_agence = Integer.parseInt(request.getParameter("distance_agence"));
		int nombre_habitant = Integer.parseInt(request.getParameter("nombre_habitant"));
		try {
 			Connection conn=DBConnection.getConnection(); 
 		    String query = "UPDATE communes SET libelle =?,distance_agence =?,nbr_habitant=? WHERE id = ?";
 		    PreparedStatement pstmt = conn.prepareStatement(query);
 		    pstmt.setString(1,libelle);
 		    pstmt.setInt(2,distance_agence);
 		    pstmt.setInt(3,nombre_habitant);
 		    pstmt.setInt(4, id);
 		   int  status=pstmt.executeUpdate();
 		  if(status==1) {
 				response.sendRedirect("communes.jsp");
 			}
 			else {
 				printWriter.println("not added");
 			}
 		   conn.close();
 		    
 		} catch(Exception e) {
 			System.out.println(e);
 		} 
		
	}

}
