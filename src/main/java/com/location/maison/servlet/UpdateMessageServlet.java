package com.location.maison.servlet;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

@WebServlet("/UpdateMessageServlet")
public class UpdateMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String messageId = request.getParameter("messageId");
        
        // Mettre à jour le champ "vue" du message avec la valeur 1
        try {
            Connection con = DBConnection.getConnection();
            String query = "UPDATE messages SET vue = 1 WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, Integer.parseInt(messageId));
            stmt.executeUpdate();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        response.sendRedirect("messages.jsp");
    }
}
