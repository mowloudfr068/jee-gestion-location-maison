package com.location.maison.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

/**
 * Servlet implementation class UpdateQuartierServlet
 */
@WebServlet("/UpdateQuartierServlet")
public class UpdateQuartierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateQuartierServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// Retrieve the submitted form data
					PrintWriter printWriter = response.getWriter();
					int id = Integer.parseInt(request.getParameter("id_quartier"));
					String libelle = request.getParameter("libelle");
					int id_communes = Integer.parseInt(request.getParameter("id_communes"));
					try {
			 			Connection conn=DBConnection.getConnection(); 
			 		    String query = "UPDATE quartiers SET libelle =?,id_communes =? WHERE id = ?";
			 		    PreparedStatement pstmt = conn.prepareStatement(query);
			 		    pstmt.setString(1,libelle);
			 		    pstmt.setInt(2,id_communes);
			 		    pstmt.setInt(3, id);
			 		   int  status=pstmt.executeUpdate();
			 		  if(status==1) {
			 				response.sendRedirect("quartiers.jsp");
			 			}
			 			else {
			 				printWriter.println("not added");
			 			}
			 		   conn.close();
			 		    
			 		} catch(Exception e) {
			 			System.out.println(e);
			 		} 
		}

}
