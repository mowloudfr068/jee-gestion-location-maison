package com.location.maison.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.location.maison.DBConnection;

/**
 * Servlet implementation class UpdateType_logementServlet
 */
@WebServlet("/UpdateType_logementServlet")
public class UpdateType_logementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateType_logementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Retrieve the submitted form data
		PrintWriter printWriter = response.getWriter();
		int id = Integer.parseInt(request.getParameter("id_type_logement"));
		String libelle = request.getParameter("libelle");
		int charge_forfetaire = Integer.parseInt(request.getParameter("charge_forfetaire"));
		try {
 			Connection conn=DBConnection.getConnection(); 
 		    String query = "UPDATE type_logements SET type_logement =?,charge_forfetaire =? WHERE id = ?";
 		    PreparedStatement pstmt = conn.prepareStatement(query);
 		    pstmt.setString(1,libelle);
 		    pstmt.setInt(2,charge_forfetaire);
 		    pstmt.setInt(3, id);
 		   int  status=pstmt.executeUpdate();
 		  if(status==1) {
 				response.sendRedirect("type_logements.jsp");
 			}
 			else {
 				printWriter.println("not added");
 			}
 		   conn.close();
 		    
 		} catch(Exception e) {
 			System.out.println(e);
 		} 
	}

}
