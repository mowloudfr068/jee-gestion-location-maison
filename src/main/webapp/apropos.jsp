<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <jsp:include page="head.jsp" />
    <title>À propos de l'agence</title>
    <style>
        /* Styles pour la section de l'agence */
        .agency-section {
            text-align: center;
            margin-top: 50px;
        }

        .agency-section h2 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .agency-section p {
            font-size: 16px;
            margin-bottom: 20px;
        }

        .agency-section img {
            max-width: 100%;
            height: auto;
            margin-bottom: 20px;
        }

        .contact-info {
            font-size: 16px;
            margin-bottom: 10px;
        }
    </style>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- ----------------------------NAVBAR PAGES------------------------------- -->

    <jsp:include page="navbarVisiteur.jsp" />
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->

    <div class="agency-section">
        <h2>À propos de l'agence</h2>
        <p>Agence Immobilière est une agence spécialisée dans la location de maisons et la recherche de biens immobiliers. Nous sommes là pour vous aider à trouver la maison idéale qui répond à tous vos besoins et vos préférences.

Notre équipe d'experts en immobilier est dévouée à vous offrir un service personnalisé et de qualité. Nous comprenons l'importance de trouver un lieu de résidence qui correspond à votre style de vie et à votre budget. Que vous recherchiez une maison familiale spacieuse, un appartement confortable ou une villa de luxe, nous avons une large sélection de biens immobiliers disponibles.</p>
        <img src="app-assets/images/ico/logoAgenceLocation.png" alt="Agence immobilière">

        <div class="contact-info">
            <p>Numéro de téléphone : +22249874807</p>
            <p><a href="https://wa.me/22249874807">WhatsApp : 22249874807</a></p>
            <p><a href="https://www.facebook.com/agence_immob">Facebook : Agence Immobilier</a></p>
        </div>
    </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
    <jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->

    <jsp:include page="scripts.jsp" />
</body>
</html>
