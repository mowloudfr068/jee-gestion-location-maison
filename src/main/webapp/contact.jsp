<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>Insert title here</title>
<style>
/* Styles pour la liste des logements */
.logements-list {
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    justify-content: flex-start; /* Alignement � gauche */
    padding: 20px; /* Ajout de l'espace autour de la liste */
}

/* Styles pour chaque logement */
.logement-card {
    width: 300px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 5px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    padding: 20px;
    margin-bottom: 20px;
}

/* Styles pour l'image du logement */
.logement-image-wrapper {
    height: 200px;
    width: 100%;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
}

.logement-image-wrapper img {
    max-width: 100%;
    max-height: 100%;
    object-fit: contain;
}

/* Styles pour les d�tails du logement */
.logement-details h2 {
    font-size: 16px;
    margin: 10px 0;
}

.logement-details p {
    margin: 5px 0;
}

/* Styles pour le bouton "Voir les d�tails" */
.logement-action button {
    background-color: #ffcc33;
    color: #fff;
    border: none;
    border-radius: 3px;
    padding: 5px 10px;
    cursor: pointer;
}

hr {
    margin: 20px 0;
    border: none;
    border-top: 1px solid #ddd;
}

.page-title {
    color: red;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
}

.animate-title {
    color: red;
    text-align: center;
    overflow: hidden;
    white-space: nowrap;
}

.animate-title::after {
    content: attr(data-text);
    display: inline-block;
    overflow: hidden;
}

@keyframes type-animation {
    from {
        width: 0;
    }
    to {
        width: 100%;
    }
}

.button {
    display: inline-block;
    padding: 10px 20px;
    background-color: #007bff;
    color: #fff;
    text-decoration: none;
    border-radius: 3px;
    transition: background-color 0.3s;
}

.button:hover {
    background-color: #0056b3;
}



</style>
<script>
document.addEventListener('DOMContentLoaded', (event) => {
    const titleElement = document.querySelector('.page-title');
    const titleText = titleElement.textContent;
    titleElement.textContent = '';

    let index = 0;
    
    const animateTitle = () => {
        titleElement.textContent = titleText.substr(0, index);
        index++;

        if (index > titleText.length) {
            index = 0;
        }
    };

    animateTitle(); // Animation initiale

    setInterval(animateTitle, 100); // R�p�ter l'animation toutes les 3 secondes
});
</script>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- ----------------------------NAVBAR PAGES------------------------------- -->

    <jsp:include page="navbarVisiteur.jsp" />
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->
<div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
        <!-- ICI CEST LE CONTENU -->
        <!-- --------page table.html -->
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h1 class="text-center">Contactez administrateur</h1>
            <form method="post" action="SaveMessageServlet">
                <div class="form-group">
                    <label for="inputNom">Nom</label>
                    <input type="text" class="form-control" id="inputNom"  name="nom" placeholder="Entrez votre nom">
                </div>
                <div class="form-group">
                    <label for="inputPrenom">Titre</label>
                    <input type="text" class="form-control" id="inputPrenom" name="titre"  placeholder="Entrez votre titre">
                </div>
                <div class="form-group">
                    <label for="inputMotDePasse">Telephone</label>
                    <input type="number" class="form-control" id="inputMotDePasse" name="telephone"  placeholder="Entrez votre telephone">
                </div>
                <div class="form-group">
                    <label for="inputAdresseEmail">Description</label><br>
                    <textarea rows="5" cols="50" name="description">
                    </textarea>
                </div>
                <button type="submit" class="btn btn-primary">Envoiyer</button>
            </form>
        </div>
    </div>
</div>
        <!-- -------end page table.html-------- -->
    </div>
    </div>
    </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
    <jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var title = document.getElementById('animated-title');
        
        function animateTitle() {
            title.classList.add('animate-title');
            setTimeout(function() {
                title.classList.remove('animate-title');
            }, 3000);
        }
        
        setInterval(animateTitle, 3000);
    });
</script>
</body>
</html>