<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
		if (session == null || session.getAttribute("name") == null) {
		response.sendRedirect("login.jsp");
		return;
}
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>Insert title here</title>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
        <!-- ----------------------------NAVBAR PAGES------------------------------- -->

	<jsp:include page="navbar.jsp"/>
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->
    <!-- ----------------------------SIDEBAR PAGES------------------------------- -->
    <jsp:include page="sidebar.html"/>

    <!-- ----------------------------END SIDEBAR PAGES------------------------------- -->

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- ICI CEST LE CONTENU -->
                <!-- --------page table.html -->
				<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header mb-1">
                <h4 class="card-title">Créer Nouveau logements</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="mb-0">
                        <a href="logements.jsp" class="btn btn-primary">
                            <span class="icon text-white-50">
                                <i class="la la-home"></i>
                            </span>
                            <span class="text">
                                logements
                            </span>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="container">
                    <form class="form" action="StoreLogementServlet" method="post" enctype="multipart/form-data">
                        <!-- Les champs du formulaire -->
                        <div class="form-body">
                            <h4 class="form-section"></h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="libelle">Libellé :</label>
        								<input type="text" name="libelle" value=""  class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Adresse :</label>
        								<input type="text" name="address" required class="form-control">
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Superficie :</label>
        								<input type="text" name="superficie" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="loyer">Loyer :</label>
        								<input type="number" name="loyer" required class="form-control">
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="status">quartier</label>
                                        <select name="quartier" class="form-control">
                                            <option value="" disabled="disabled" >
                                                selectionner le quartier</option>
                                            <%
			    try {
			    	Connection con = DBConnection.getConnection();
			        Statement st = con.createStatement();
			        String query = "SELECT * FROM quartiers";
			        ResultSet rs = st.executeQuery(query);
			        while(rs.next()) {
					%>
                   <option value="<%=rs.getString("id") %>" selected><%=rs.getString("libelle") %></option>
                   <%
        }
			        con.close();
			    } catch(Exception e) {
			        out.println(e.getMessage());
			    }
%>
                                        </select>
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="status">type logement</label>
                                        <select name="typeLogement" class="form-control">
                                            <option value="" disabled="disabled" >
                                                selectionner le type de logement</option>
                                            <%
			    try {
			    	Connection con = DBConnection.getConnection();
			        Statement st = con.createStatement();
			        String query = "SELECT * FROM type_logements";
			        ResultSet rs = st.executeQuery(query);
			        while(rs.next()) {
					%>
                   <option value="<%=rs.getString("id") %>" selected><%=rs.getString("type_logement") %></option>
                   <%
        }
			        con.close();
			    } catch(Exception e) {
			        out.println(e.getMessage());
			    }
%>
                                        </select>
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="status">statut</label>
                                        <select name="status" class="form-control">
                                            <option value="" disabled="disabled" >
                                            selectionner le statut
                                          </option>
                                          <option value="0" >
                                            disponible
                                          </option>
                                          <option value="1">
                                            
                                            occupper
                                          </option>
                                          </select>
                                          </div>
                                          </div>
                                          </div>
                                          <div class="row">
                            <div class="col-md-12">
                                    <div class="form-group">
                                          <p>Description :</p>
										<textarea rows="5" cols="100"  name="description" ></textarea>
                                          </div>
                                          </div>
                                          </div>
                            <div class="row">
                            <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="loyer">image de logement :</label>
        								<input type="file" name="img" required class="form-control">
                                    </div>
                                </div>
                                </div>
                        <div class="form-actions">
                            <button type="button" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Annuler
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Créer
                            </button>
                        </div>
                        </div>
                    </form>
                </div>
               
            </div>
        </div>
    </div>
</div>
                        </div>
                <!-- -------end page table.html-------- -->
            </div>
        </div>
       </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
	<jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->
    <jsp:include page="modalLogout.html" />

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>