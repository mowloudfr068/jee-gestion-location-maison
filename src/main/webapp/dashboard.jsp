<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
    if (session == null || session.getAttribute("name") == null) {
        response.sendRedirect("login.jsp");
        return;
    }
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>A propos de l'agence</title>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
        <!-- ----------------------------NAVBAR PAGES------------------------------- -->

    <jsp:include page="navbar.jsp"/>
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->
    <!-- ----------------------------SIDEBAR PAGES------------------------------- -->
    <jsp:include page="sidebar.html"/>

    <!-- ----------------------------END SIDEBAR PAGES------------------------------- -->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <style>
                    .icon-large {
                        font-size: 30px;
                    }
                </style>

                <div class="card-header">
                    Statistiques
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div class="card h-100 bg-info text-white">
                                <div class="card-body text-center">
                                    <i class="la la-comments text-white m-lg-2 icon-large" aria-hidden="true"></i>
                                    <h5 class="card-title">Messages</h5>
                                    <%
                                        try {
                                            Connection con = DBConnection.getConnection();
                                            Statement st = con.createStatement();
                                            String query = "SELECT COUNT(*) AS totalMessages FROM messages WHERE vue = 0";
                                            ResultSet rs = st.executeQuery(query);
                                            if (rs.next()) {
                                                int totalMessages = rs.getInt("totalMessages");
                                    %>
                                    <div class="card-text"><%= totalMessages %></div>
                                    <%
                                            }
                                            con.close();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="card h-100 bg-warning text-white">
                                <div class="card-body text-center">
                                    <i class="la la-building text-white m-lg-2 icon-large" aria-hidden="true"></i>
                                    <h5 class="card-title">Communes</h5>
                                    <%
                                        try {
                                            Connection con = DBConnection.getConnection();
                                            Statement st = con.createStatement();
                                            String query = "SELECT COUNT(*) AS totalCommunes FROM communes";
                                            ResultSet rs = st.executeQuery(query);
                                            if (rs.next()) {
                                                int totalCommunes = rs.getInt("totalCommunes");
                                    %>
                                    <div class="card-text"><%= totalCommunes %></div>
                                    <%
                                            }
                                            con.close();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="card h-100 bg-danger text-white">
                                <div class="card-body text-center">
                                    <i class="la la-map-marker text-white m-lg-2 icon-large" aria-hidden="true"></i>
                                    <h5 class="card-title">Quartiers</h5>
                                    <%
                                        try {
                                            Connection con = DBConnection.getConnection();
                                            Statement st = con.createStatement();
                                            String query = "SELECT COUNT(*) AS totalQuartiers FROM quartiers";
                                            ResultSet rs = st.executeQuery(query);
                                            if (rs.next()) {
                                                int totalQuartiers = rs.getInt("totalQuartiers");
                                    %>
                                    <div class="card-text"><%= totalQuartiers %></div>
                                    <%
                                            }
                                            con.close();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div class="card h-100 bg-success text-white">
                                <div class="card-body text-center">
                                    <i class="la la-home text-white m-lg-2 icon-large" aria-hidden="true"></i>
                                    <h5 class="card-title">Types de logements</h5>
                                    <%
                                        try {
                                            Connection con = DBConnection.getConnection();
                                            Statement st = con.createStatement();
                                            String query = "SELECT COUNT(*) AS totalTypesLogements FROM type_logements";
                                            ResultSet rs = st.executeQuery(query);
                                            if (rs.next()) {
                                                int totalTypesLogements = rs.getInt("totalTypesLogements");
                                    %>
                                    <div class="card-text"><%= totalTypesLogements %></div>
                                    <%
                                            }
                                            con.close();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="card h-100 text-white" style="background-color: darkgray">
                                <div class="card-body text-center">
                                    <i class="la la-building-o text-white m-lg-2 icon-large" aria-hidden="true"></i>
                                    <h5 class="card-title">Logements</h5>
                                    <%
                                        try {
                                            Connection con = DBConnection.getConnection();
                                            Statement st = con.createStatement();
                                            String query = "SELECT COUNT(*) AS totalLogements FROM logements";
                                            ResultSet rs = st.executeQuery(query);
                                            if (rs.next()) {
                                                int totalLogements = rs.getInt("totalLogements");
                                    %>
                                    <div class="card-text"><%= totalLogements %></div>
                                    <%
                                            }
                                            con.close();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
    <jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->
    <jsp:include page="modalLogout.html" />

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>
