<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>

<!DOCTYPE html>
<html>
<head>
    <jsp:include page="head.jsp" />
    <title>Détails du logement</title>
    <style>
        .logement-details {
            text-align: center;
            margin: 20px;
        }

        .logement-details h2 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .logement-details p {
            font-size: 16px;
            margin-bottom: 5px;
        }

        .logement-image-wrapper {
            margin-top: 20px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .logement-image-wrapper img {
            max-width: 100%;
            height: auto;
        }

        .contact-details {
            text-align: center;
            margin-top: 30px;
        }

        .contact-details h3 {
            font-size: 20px;
            margin-bottom: 10px;
        }

        .contact-details hr {
            margin: 20px 0;
            border: none;
            border-top: 1px solid #ddd;
        }

        .contact-details p {
            font-size: 16px;
            margin-bottom: 5px;
        }

        .card {
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            padding: 20px;
        }
        
        .page-title {
    color: red;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
}

.animate-title {
    color: red;
    text-align: center;
    overflow: hidden;
    white-space: nowrap;
}

.animate-title::after {
    content: attr(data-text);
    display: inline-block;
    overflow: hidden;
}

@keyframes type-animation {
    from {
        width: 0;
    }
    to {
        width: 100%;
    }
}
    </style>
    <script>
document.addEventListener('DOMContentLoaded', (event) => {
    const titleElement = document.querySelector('.page-title');
    const titleText = titleElement.textContent;
    titleElement.textContent = '';

    let index = 0;
    
    const animateTitle = () => {
        titleElement.textContent = titleText.substr(0, index);
        index++;

        if (index > titleText.length) {
            index = 0;
        }
    };

    animateTitle(); // Animation initiale

    setInterval(animateTitle, 100); // Répéter l'animation toutes les 3 secondes
});
</script>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu-modern" data-col="2-columns">
<!-- ----------------------------NAVBAR PAGES------------------------------- -->
<jsp:include page="navbarVisiteur.jsp" />
<!-- ----------------------------END NAVBAR PAGES------------------------------- -->

<div class="logement-details">
<h3 class="page-title" id="animated-title">Nous vous proposons les meilleures offres....! نقدم لك أفضل العروض</h3>
    <div class="card">
        <div class="logement-details-center">
            <%
                try {
                    Connection con = DBConnection.getConnection();
                    Statement st = con.createStatement();
                    String logementId = request.getParameter("id");
                    String query = "SELECT l.*, q.libelle AS quartier, c.libelle AS commune, t.type_logement as type_logement,t.charge_forfetaire as charge_forfetaire FROM logements l JOIN quartiers q ON l.id_quartiers = q.id JOIN communes c ON q.id_communes = c.id JOIN type_logements t ON l.id_type_logements = t.id WHERE l.id = " + logementId;
                    ResultSet rs = st.executeQuery(query);
                    if (rs.next()) {
                        String libelle = rs.getString("libelle");
                        String address = rs.getString("address");
                        int superficie = rs.getInt("superficie");
                        int loyerFinal = rs.getInt("loyer")+rs.getInt("charge_forfetaire");
                        String typeLogement = rs.getString("type_logement");
                        String quartier = rs.getString("quartier");
                        String commune = rs.getString("commune");
                        String description = rs.getString("description");
                        String imageFileName = rs.getString("img");
            %>
            <h2>Détails du logement : <%= libelle %></h2>
            <p>Quartier : <%= quartier %></p>
            <p>Commune : <%= commune %></p>
            <p>Type de logement : <%= typeLogement %></p>
            <p>Loyer : <%= loyerFinal %> UM</p>
            <p>Description : <%= description %> .</p>
            <div class="logement-image-wrapper">
                <img src="images/<%= imageFileName %>" alt="Image du logement">
            </div>
            <% 
                    } else {
                        out.println("Logement non trouvé.");
                    }
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            %>
        </div>
        <div class="contact-details">
    <hr>
    <h3>Contactez 49874807</h3>
    <hr>
</div>
    </div>
    
</div>



<jsp:include page="scripts.jsp" />
</body>
</html>
