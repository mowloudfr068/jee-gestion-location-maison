<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
		if (session == null || session.getAttribute("name") == null) {
		response.sendRedirect("login.jsp");
		return;
}
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>Insert title here</title>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
        <!-- ----------------------------NAVBAR PAGES------------------------------- -->

	<jsp:include page="navbar.jsp"/>
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->
    <!-- ----------------------------SIDEBAR PAGES------------------------------- -->
    <jsp:include page="sidebar.html"/>

    <!-- ----------------------------END SIDEBAR PAGES------------------------------- -->

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- ICI CEST LE CONTENU -->
                <!-- --------page table.html -->
				<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header mb-1">
                <h4 class="card-title">modifier commune</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="mb-0">
                        <a href="communes.jsp" class="btn btn-primary">
                            <span class="icon text-white-50">
                                <i class="la la-home"></i>
                            </span>
                            <span class="text">
                                Communes
                            </span>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="container">
                <% try {
                	Connection con = DBConnection.getConnection();
                    Statement st = con.createStatement();
                    int id = Integer.parseInt(request.getParameter("id_commune"));
                    String query = "SELECT * FROM communes WHERE id=" + id;
                    ResultSet rs = st.executeQuery(query);
                    if (rs.next()) {
                        // Retrieve the commune record and display the form to edit it
                        int id_commune = rs.getInt("id");
                        String libelle = rs.getString("libelle");
                        int distance_agence = rs.getInt("distance_agence");
                        int nombre_habitant = rs.getInt("nbr_habitant");
                %>
                    <form class="form" action="UpdateCommuneServlet" method="post">
                    <input type="hidden" name="id_commune" value="<%=id_commune %>">
                        <!-- Les champs du formulaire -->
                        <div class="form-body">
                            <h4 class="form-section"></h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name_fr">Libellé</label>
                                        <input type="text" name="libelle" value="<%=libelle  %>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name_ar">distance agence</label>
                                        <input type="text" name="distance_agence" value="<%=distance_agence  %>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="nombre_habitant">Nombre d'habitants</label>
                                        <input type="text" name="nombre_habitant" value="<%=nombre_habitant  %>" class="form-control">
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <button type="button" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Annuler
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Créer
                            </button>
                        </div>
                    </form>
                    <% } else {
                    // commune not found, display an error message
                    out.println("commune not found");
                }
                con.close();
            } catch(Exception e) {
                out.println(e.getMessage());
            } %>
                </div>
            </div>
        </div>
    </div>
</div>
                        </div>
                <!-- -------end page table.html-------- -->
            </div>
        </div>
    </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
	<jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->
    <jsp:include page="modalLogout.html" />

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>