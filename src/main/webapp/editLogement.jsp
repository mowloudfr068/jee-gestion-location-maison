<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
		if (session == null || session.getAttribute("name") == null) {
		response.sendRedirect("login.jsp");
		return;
}
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>Insert title here</title>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
        <!-- ----------------------------NAVBAR PAGES------------------------------- -->

	<jsp:include page="navbar.jsp"/>
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->
    <!-- ----------------------------SIDEBAR PAGES------------------------------- -->
    <jsp:include page="sidebar.html"/>

    <!-- ----------------------------END SIDEBAR PAGES------------------------------- -->

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- ICI CEST LE CONTENU -->
                <!-- --------page table.html -->
				<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header mb-1">
                <h4 class="card-title">Modifier logement</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="mb-0">
                        <a href="logements.jsp" class="btn btn-primary">
                            <span class="icon text-white-50">
                                <i class="la la-home"></i>
                            </span>
                            <span class="text">
                                Logements
                            </span>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="container">
                <% try {
                	Connection con = DBConnection.getConnection();
                    Statement st = con.createStatement();
                    int id = Integer.parseInt(request.getParameter("id_logement"));
                    String query = "SELECT * FROM logements WHERE id=" + id;
                    ResultSet rs = st.executeQuery(query);
                    if (rs.next()) {
                        // Retrieve the logement record and display the form to edit it
                        int id_logement = rs.getInt("id");
                        String libelle = rs.getString("libelle");
                        String address = rs.getString("address");
                        int superficie = rs.getInt("superficie");
                        int loyer = rs.getInt("loyer");
                        int id_quartier = rs.getInt("id_quartiers");
                        int id_type_logement = rs.getInt("id_type_logements");
                        String imageFileName = rs.getString("img");
                %>
                    <form class="form" action="UpdateLogementServlet" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_logement" value="<%= id_logement %>">
                        <!-- Les champs du formulaire -->
                        <div class="form-body">
                            <h4 class="form-section"></h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="libelle">Libellé :</label>
                                        <input type="text" name="libelle" value="<%= libelle %>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Adresse :</label>
                                        <input type="text" name="address" value="<%= address %>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="superficie">Superficie :</label>
                                        <input type="number" name="superficie" value="<%= superficie %>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="loyer">Loyer :</label>
                                        <input type="number" name="loyer" value="<%= loyer %>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="quartier">Quartier :</label>
                                        <select name="quartier" class="form-control">
                                            <% 
                                            try {
                                                Connection connection = DBConnection.getConnection();
                                                Statement statement = connection.createStatement();
                                                String queryString = "SELECT * FROM quartiers";
                                                ResultSet resultSet = statement.executeQuery(queryString);
                                                while (resultSet.next()) {
                                                    int idQuartier = resultSet.getInt("id");
                                                    String nomQuartier = resultSet.getString("libelle");
                                                    %>
                                                    <option value="<%= idQuartier %>"
                                                        <% if (id_quartier == idQuartier) { %> selected <% } %>>
                                                        <%= nomQuartier %>
                                                    </option>
                                                    <% 
                                                }
                                                connection.close();
                                            } catch (Exception e) {
                                                out.println(e.getMessage());
                                            }
                                            %>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="typeLogement">Type de logement :</label>
                                        <select name="typeLogement" class="form-control">
                                            <% 
                                            try {
                                                Connection connection = DBConnection.getConnection();
                                                Statement statement = connection.createStatement();
                                                String queryString = "SELECT * FROM type_logements";
                                                ResultSet resultSet = statement.executeQuery(queryString);
                                                while (resultSet.next()) {
                                                    int idTypeLogement = resultSet.getInt("id");
                                                    String nomTypeLogement = resultSet.getString("type_logement");
                                                    %>
                                                    <option value="<%= idTypeLogement %>"
                                                        <% if (id_type_logement == idTypeLogement) { %> selected <% } %>>
                                                        <%= nomTypeLogement %>
                                                    </option>
                                                    <% 
                                                }
                                                connection.close();
                                            } catch (Exception e) {
                                                out.println(e.getMessage());
                                            }
                                            %>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="status">statut</label>
                                        <select name="status" class="form-control">
									  <option value="" disabled="disabled" >
									    Sélectionner le statut
									  </option>
									  <option value="0" <% if (rs.getInt("status") == 0) { %>selected<% } %>>
									    Disponible
									  </option>
									  <option value="1" <% if (rs.getInt("status") == 1) { %>selected<% } %>>
									    Occupé
									  </option>
										</select>
                                          </div>
                                          </div>
                                          </div>
                                          <div class="row">
                            <div class="col-md-12">
                                    <div class="form-group">
                                          <p>Description :</p>
										<textarea rows="5" cols="100"  name="description">
										<%=rs.getString("description") %>
										
										</textarea>
                                          </div>
                                          </div>
                                          </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="img">Image :</label>
                                        <input type="file" name="img"   class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Annuler
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Modifier
                            </button>
                        </div>
                    </form>
                    
                    <% } else {
                    // logement not found, display an error message
                    out.println("Logement not found");
                }
                con.close();
            } catch(Exception e) {
                out.println(e.getMessage());
            } %>
                </div>
            </div>
        </div>
    </div>
</div>
                <!-- -------end page table.html-------- -->
            </div>
        </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
	<jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->
    <jsp:include page="modalLogout.html" />

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>