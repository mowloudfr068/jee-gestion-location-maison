<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
		if (session == null || session.getAttribute("name") == null) {
		response.sendRedirect("login.jsp");
		return;
}
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>Insert title here</title>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
        <!-- ----------------------------NAVBAR PAGES------------------------------- -->

	<jsp:include page="navbar.jsp"/>
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->
    <!-- ----------------------------SIDEBAR PAGES------------------------------- -->
    <jsp:include page="sidebar.html"/>

    <!-- ----------------------------END SIDEBAR PAGES------------------------------- -->

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- ICI CEST LE CONTENU -->
                <!-- --------page table.html -->
				<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header mb-1">
                <h4 class="card-title">modifier quartier</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="mb-0">
                        <a href="quartiers.jsp" class="btn btn-primary">
                            <span class="icon text-white-50">
                                <i class="la la-home"></i>
                            </span>
                            <span class="text">
                                quartiers
                            </span>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="container">
                <% try {
                	Connection con = DBConnection.getConnection();
                    Statement st = con.createStatement();
                    int id = Integer.parseInt(request.getParameter("id_quartier"));
                    String query = "SELECT * FROM quartiers WHERE id=" + id;
                    ResultSet rs = st.executeQuery(query);
                    if (rs.next()) {
                        // Retrieve the quartier record and display the form to edit it
                        int id_quartier = rs.getInt("id");
                        String libelle = rs.getString("libelle");
                        int id_commune = rs.getInt("id_communes");
                %>
                    <form class="form" action="UpdateQuartierServlet" method="post">
                    <input type="hidden" name="id_quartier" value="<%=id_quartier %>">
                        <!-- Les champs du formulaire -->
                        <div class="form-body">
                            <h4 class="form-section"></h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name_fr">Libellé</label>
                                        <input type="text" name="libelle" value="<%=libelle  %>" class="form-control">
                                    </div>
                                </div>
                                </div>
                                <div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="commune_id">Commune</label>
            <select name="id_communes" class="form-control">
                <option value="" disabled selected>Selectionnez la commune</option>
                <% 
                try {
                    Connection connection = DBConnection.getConnection();
                    Statement statement = connection.createStatement();
                    String queryString = "SELECT * FROM communes";
                    ResultSet resultSet = statement.executeQuery(queryString);
                    while (resultSet.next()) {
                        int idCommune = resultSet.getInt("id");
                        String libelleCommune = resultSet.getString("libelle");
                        %>
                        <option value="<%= idCommune %>"
                            <% if (id_commune == idCommune) { %> selected <% } %>>
                            <%= libelleCommune %>
                        </option>
                        <% 
                    }
                    connection.close();
                } catch (Exception e) {
                    out.println(e.getMessage());
                }
                %>
            </select>
        </div>
    </div>
</div>

                        <div class="form-actions">
                            <button type="button" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Annuler
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Créer
                            </button>
                        </div>
                    </form>
                    
                    <% } else {
                    // quartier not found, display an error message
                    out.println("quartier not found");
                }
                con.close();
            } catch(Exception e) {
                out.println(e.getMessage());
            } %>
                </div>
            </div>
        </div>
    </div>
</div>
                        </div>
                <!-- -------end page table.html-------- -->
            </div>
        </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
	<jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->
    <jsp:include page="modalLogout.html" />

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>