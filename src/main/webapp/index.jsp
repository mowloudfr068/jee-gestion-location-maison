<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
    if (session == null || session.getAttribute("name") == null) {
        response.sendRedirect("login.jsp");
        return;
    }
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>Insert title here</title>
<style>
/* Styles pour la liste des logements */
.logements-list {
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    justify-content: flex-start; /* Alignement à gauche */
    padding: 20px; /* Ajout de l'espace autour de la liste */
}

/* Styles pour chaque logement */
.logement-card {
    width: 300px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 5px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    padding: 20px;
    margin-bottom: 20px;
}

/* Styles pour l'image du logement */
.logement-image-wrapper {
    height: 200px;
    width: 100%;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
}

.logement-image-wrapper img {
    max-width: 100%;
    max-height: 100%;
    object-fit: contain;
}

/* Styles pour les détails du logement */
.logement-details h2 {
    font-size: 16px;
    margin: 10px 0;
}

.logement-details p {
    margin: 5px 0;
}

/* Styles pour le bouton "Voir les détails" */
.logement-action button {
    background-color: #ffcc33;
    color: #fff;
    border: none;
    border-radius: 3px;
    padding: 5px 10px;
    cursor: pointer;
}

hr {
    margin: 20px 0;
    border: none;
    border-top: 1px solid #ddd;
}

.page-title {
    color: red;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
}

.animate-title {
    color: red;
    text-align: center;
    overflow: hidden;
    white-space: nowrap;
}

.animate-title::after {
    content: attr(data-text);
    display: inline-block;
    overflow: hidden;
}

@keyframes type-animation {
    from {
        width: 0;
    }
    to {
        width: 100%;
    }
}

.button {
    display: inline-block;
    padding: 10px 20px;
    background-color: #007bff;
    color: #fff;
    text-decoration: none;
    border-radius: 3px;
    transition: background-color 0.3s;
}

.button:hover {
    background-color: #0056b3;
}



</style>
<script>
document.addEventListener('DOMContentLoaded', (event) => {
    const titleElement = document.querySelector('.page-title');
    const titleText = titleElement.textContent;
    titleElement.textContent = '';

    let index = 0;
    
    const animateTitle = () => {
        titleElement.textContent = titleText.substr(0, index);
        index++;

        if (index > titleText.length) {
            index = 0;
        }
    };

    animateTitle(); // Animation initiale

    setInterval(animateTitle, 100); // Répéter l'animation toutes les 3 secondes
});
</script>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- ----------------------------NAVBAR PAGES------------------------------- -->

    <jsp:include page="navbarVisiteur.jsp" />
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->

    <div class="content-body">
        <!-- ICI CEST LE CONTENU -->
        <!-- --------page table.html -->
        <h3 class="page-title" id="animated-title">Nous vous proposons les meilleures offres....! نقدم لك أفضل العروض</h3>


        <div class="logements-list">
            <%
            try {
                Connection con = DBConnection.getConnection();
                Statement st = con.createStatement();
                String query = "SELECT l.*, q.libelle AS quartier, c.libelle AS commune, t.type_logement AS type_logement, t.charge_forfetaire AS charge_forfetaire FROM logements l JOIN quartiers q ON l.id_quartiers = q.id JOIN communes c ON q.id_communes = c.id JOIN type_logements t ON l.id_type_logements = t.id WHERE l.status = 0";
                ResultSet rs = st.executeQuery(query);
                while (rs.next()) {
                    String libelle = rs.getString("libelle");
                    int loyerFinal = rs.getInt("loyer")+rs.getInt("charge_forfetaire");
                    String typeLogement = rs.getString("type_logement");
                    String quartier = rs.getString("quartier");
                    String commune = rs.getString("commune");
                    String imageFileName = rs.getString("img");
            %>
            <div class="logement-card">
                <div class="logement-image-wrapper">
                    <img src="images/<%=imageFileName %>" alt="Image du logement">
                </div>
                <div class="logement-details">
                    <h2><%= libelle %></h2>
                    <p>Loyer : <%= loyerFinal %> UM</p>
                    <p>Type de logement : <%= typeLogement %></p>
                    <p>Quartier : <%= quartier %></p>
                    <p>Commune : <%= commune %></p>
                </div>
                <div class="logement-action">
    <a class="button" href="detailsLogement.jsp?id=<%= rs.getInt("id") %>">Voir les détails</a>
</div>

            </div>
            <% 
                }
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            %>
        </div>
        <!-- -------end page table.html-------- -->
    </div>
    

    <!-- ----------------------------FOOTER PAGES------------------------------- -->

    <jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var title = document.getElementById('animated-title');
        
        function animateTitle() {
            title.classList.add('animate-title');
            setTimeout(function() {
                title.classList.remove('animate-title');
            }, 3000);
        }
        
        setInterval(animateTitle, 3000);
    });
</script>
</body>
</html>