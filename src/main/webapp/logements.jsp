<%@page import="java.sql.Blob"%>
<%@page import="java.util.Base64"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
		if (session == null || session.getAttribute("name") == null) {
		response.sendRedirect("login.jsp");
		return;
}
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp"></jsp:include>
<title>Insert title here</title>
<style type="text/css">
.description-cell {
  max-width: 300px; /* Ajustez la valeur selon vos besoins */
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

.description-content {
  overflow: auto;
}
</style>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
        <!-- ----------------------------NAVBAR PAGES------------------------------- -->

	<jsp:include page="navbar.jsp"/>
    <!-- ----------------------------END NAVBAR PAGES------------------------------- -->
    <!-- ----------------------------SIDEBAR PAGES------------------------------- -->
    <jsp:include page="sidebar.html"/>

    <!-- ----------------------------END SIDEBAR PAGES------------------------------- -->

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- ICI CEST LE CONTENU -->
                <!-- --------page table.html -->
				<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header mb-1">
                <h4 class="card-title">List Des logements</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="mb-0">
                        <a href="createLogement.jsp" class="btn btn-primary">
                            <span class="icon text-white-50">
                                <i class="la la-plus"></i>
                            </span>
                            <span class="text">Créer Nouveau logement</span>
                        </a>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="table-responsive">
                <%
			    try {
			    	Connection con = DBConnection.getConnection();
			        Statement st = con.createStatement();
			        String query = "SELECT l.*, q.libelle AS quartier, c.libelle AS commune,t.type_logement as type_logement FROM logements l JOIN quartiers q ON l.id_quartiers = q.id JOIN communes c ON q.id_communes = c.id JOIN type_logements t ON l.id_type_logements = t.id";
			        ResultSet rs = st.executeQuery(query);
					%>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            	<th scope="col">image</th>
                                <th scope="col">libelle</th>
                                <th scope="col">address</th>
                                <th scope="col">superficie</th>
                                <th scope="col">loyer</th>
                                <th scope="col">type logement</th>
                                <th scope="col">quartier</th>
                                <th scope="col">commune</th>
                                <th scope="col">status</th>
                                <th scope="col">description</th>
                                <th scope="col" class="text-center" style="width: 30px;">Opérations</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Boucle pour afficher les communes -->
                            <%
        while(rs.next()) {
        	
        	
        	
            String imageUrl = rs.getString("img");
            // Ajoutez un paramètre aléatoire à l'URL de l'image
            String imageUrlWithRandomParam = imageUrl + "?rand=" + Math.random();
       
%>
                            <tr>
                               <td><img src="images/<%=imageUrlWithRandomParam%>" style="width:100px;height:100px"></td>
                                <td><%=rs.getString("libelle")%></td>
                                <td><%=rs.getString("address") %></td>
                                <td><%=rs.getString("superficie") %></td>
                                <td><%=rs.getString("loyer") %></td>
                                <td><%=rs.getString("type_logement") %></td>
                                <td><%=rs.getString("quartier") %></td>
                                <td><%=rs.getString("commune") %></td>
                                <td>
								  <% String status = rs.getString("status"); %>
								  <span class="<%= status.equals("1") ? "text-danger" : "text-success" %>">
								    <%= status.equals("1") ? "Occupé" : "Disponible" %>
								  </span>
								</td>

                              <td class="description-cell">
                              <div class="description-content">
								    <%=rs.getString("description") %>
								  </div>
                              
                              </td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <form action="editLogement.jsp" method="post">
                                        <input type="hidden" name="id_logement" value="<%=rs.getString("id")%>">
                                        <button type="submit"  class="btn btn-success la la-edit">
                                        </button>
                                        </form>
                                        <form action="DeleteLogementServlet" method="post">
                                        <input type="hidden" name="id_logement" value="<%=rs.getString("id")%>">
                                        <button type="submit" onclick="return confirm('ete vous sur pour la suppression ? ')" class="btn btn-danger la la-trash">
                                        </button>
                                        </form>
                                        
                                    </div>
                                </td>
                            </tr>
                            <!-- Fin de la boucle -->
                         <!-- <tr>
                                <td colspan="6" class="text-center">Aucun quartier</td>
                            </tr> -->   
                           <%
        }
%>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="float-right">
                                        <!-- Pagination -->
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                 <%
        con.close();
    } catch(Exception e) {
        out.println(e.getMessage());
    }
%>
                </div>
            </div>
        </div>
    </div>
</div>
                <!-- -------end page table.html-------- -->
            </div>
        </div>
    </div>

    <!-- ----------------------------FOOTER PAGES------------------------------- -->
    
	<jsp:include page="footer.html" />
    <!-- ----------------------------END FOOTER PAGES------------------------------- -->
    <!----------------------------- MODAL PAGES------------------------------- -->
        <!-- Logout Modal-->
    <jsp:include page="modalLogout.html" />

    <!-- ---------------------------- END MODAL PAGES------------------------------- -->

<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>