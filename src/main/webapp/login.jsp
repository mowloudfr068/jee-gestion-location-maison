<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
         content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords"
        content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>
        app front
    </title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/defaultLogoRimPub.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/defaultLogoRimPub.jpg">
     <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
        rel="stylesheet">
    <link href="app-assets/fonts/googlefonts.css" rel="stylesheet">
     <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <link href="app-assets/fonts/line-awesome/css/line-awesome.min.css" rel="stylesheet">

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css"
        href="app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css"
        href="app-assets/css/app.css">
    <link href="app-assets/fonts/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css"
        href="app-assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css"
        href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css"
        href="app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css"
        href="app-assets/css/core/colors/palette-gradient.css">

    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.min.css">
    <!-- END Custom CSS-->
<link rel="stylesheet" href="app-assets/sweetalert-dist/sweetalert2.min.css">
<style type="text/css">

.bg-login-image {
    background: url(app-assets/images/login01.jpg);
    background-position: center;
    background-size: cover;
}
</style>
<title>Insert title here</title>
</head>
<body>
<input type="hidden" id="status" value= "<%=request.getAttribute("status")%>">
<div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
            
            
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                    <div class="col-lg-6">
                        <div class="p-5">
                            <div class="text-center">
                            	
                                <h1 class="h4 text-gray-900 mb-4">BienVenue !</h1>
                                <%
				            if(request.getAttribute("status") !=null && request.getAttribute("status").equals("failed")){
				            	out.println("<div style='color:red;'>Nom d’utilisateur ou mot de passe incorrect</div>");
				            }else if(request.getAttribute("status") !=null && request.getAttribute("status").equals("InvalidUsername")){
				            	out.println("<div style='color:red;'>Veuillez entrer le nom d’utilisateur</div>");
				            }else if(request.getAttribute("status") !=null && request.getAttribute("status").equals("InvalidPassword")){
				            	out.println("<div style='color:red;'>Veuillez entrer le mot de passe</div>");
				            }
            
           						%>
                            </div>
                            <form class="user" method="post" action=" LoginServlet">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control form-control-user"
                                        id="exampleInputEmail" aria-describedby="emailHelp"
                                        placeholder="Enter Email Address...">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control form-control-user"
                                        id="exampleInputPassword" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <div	 class="custom-control custom-checkbox small">
                                        <input type="checkbox"  class="custom-control-input" id="customCheck">
                                        <label class="custom-control-label" for="customCheck">Remember
                                            Me</label>
                                    </div>
                                </div>
                                
                                <input type="submit" class="btn btn-primary btn-user btn-block" value="Connéxion">

                            </form>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
 <!-- BEGIN VENDOR JS-->

    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
	
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/data/jvector/visitor-data.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
<script src="app-assets/sweetalert-dist/sweetalert2.min.js"></script>

</body>
</html>