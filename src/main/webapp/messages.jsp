<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<%
	if (session == null || session.getAttribute("name") == null) {
		response.sendRedirect("login.jsp");
		return;
	}
%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="head.jsp"></jsp:include>
    <title>Liste des messages</title>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu-modern" data-col="2-columns">
<!-- ----------------------------NAVBAR PAGES------------------------------- -->
<jsp:include page="navbar.jsp"/>
<!-- ----------------------------END NAVBAR PAGES------------------------------- -->
<!-- ----------------------------SIDEBAR PAGES------------------------------- -->
<jsp:include page="sidebar.html"/>
<!-- ----------------------------END SIDEBAR PAGES------------------------------- -->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- ICI CEST LE CONTENU -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header mb-1">
                            <h4 class="card-title">Liste des Messages</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="table-responsive">
                                <ul class="list-group list-group-flush">
                                    <!-- Boucle pour afficher les messages -->
                                    <% 
                                    try {
                                        Connection con = DBConnection.getConnection();
                                        Statement st = con.createStatement();
                                        String query = "SELECT id, description,created_at FROM messages WHERE vue=0 ORDER BY id DESC";
                                        ResultSet rs = st.executeQuery(query);
                                        while (rs.next()) {
                                            int messageId = rs.getInt("id");
                                            String description = rs.getString("description");
                                            java.sql.Timestamp createdAt = rs.getTimestamp("created_at");

                                         // Obtenir le temps actuel
                                         java.sql.Timestamp now = new java.sql.Timestamp(System.currentTimeMillis());

                                         // Calculer la durée écoulée en millisecondes
                                         long milliseconds = now.getTime() - createdAt.getTime();

                                         // Calculer les composantes de la durée (jours, heures, minutes, secondes)
                                         long days = milliseconds / (1000 * 60 * 60 * 24);
                                         long hours = (milliseconds % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
                                         long minutes = (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
                                         long seconds = (milliseconds % (1000 * 60)) / 1000;

                                         // Construire la chaîne de temps écoulé
                                         String elapsedTime;
                                         if (days > 0) {
                                             elapsedTime = days + " jour(s)";
                                         } else if (hours > 0) {
                                             elapsedTime = hours + " heure(s)";
                                         } else if (minutes > 0) {
                                             elapsedTime = minutes + " minute(s)";
                                         } else {
                                             elapsedTime = seconds + " seconde(s)";
                                         }


                                    %>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <%= description %> <label class="badge badge-success"><%=elapsedTime %></label>
                                        <a class="button" href="voirMessage.jsp?id=<%= messageId %>"><i class="la la-eye"></i></a>
                                    </li>
                                    <% 
                                        }
                                        con.close();
                                    } catch (Exception e) {
                                        out.println(e.getMessage());
                                    }
                                    %>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ----------------------------FOOTER PAGES------------------------------- -->
<jsp:include page="footer.html" />
<!-- ----------------------------END FOOTER PAGES------------------------------- -->
<!----------------------------- MODAL PAGES------------------------------- -->
<!-- Logout Modal-->
<jsp:include page="modalLogout.html" />
<!-- ---------------------------- END MODAL PAGES------------------------------- -->
<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>
