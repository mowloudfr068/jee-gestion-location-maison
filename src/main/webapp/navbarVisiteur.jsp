<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto">
                    <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                        <i class="ft-menu font-large-1"></i>
                    </a>
                </li>
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="#">
                        <img class="brand-logo rounded-circle" alt="Agence Location Immobilier logo" src="app-assets/images/ico/logoAgenceLocation.png">
                        <h3 class="brand-text">Agence Immobilier</h3>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">Accueil</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav float-right">
                <li class="nav-item">
                        <a class="nav-link" href="contact.jsp">message</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="apropos.jsp">� propos</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<style>
.navbar-nav .nav-item .nav-link:hover {
    color: blue;
}
</style>
