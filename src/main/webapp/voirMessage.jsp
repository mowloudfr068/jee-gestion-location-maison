<%@page import="java.sql.Statement"%>
<%@page import="com.location.maison.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<%
	if (session == null || session.getAttribute("name") == null) {
		response.sendRedirect("login.jsp");
		return;
	}
%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="head.jsp"></jsp:include>
    <title>Message</title>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu-modern" data-col="2-columns">
<!-- ----------------------------NAVBAR PAGES------------------------------- -->
<jsp:include page="navbar.jsp"/>
<!-- ----------------------------END NAVBAR PAGES------------------------------- -->
<!-- ----------------------------SIDEBAR PAGES------------------------------- -->
<jsp:include page="sidebar.html"/>
<!-- ----------------------------END SIDEBAR PAGES------------------------------- -->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- ICI CEST LE CONTENU -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header mb-1">
                            <h4 class="card-title">Détails du Message</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="table-responsive">
                                <% 
                                try {
                                    Connection con = DBConnection.getConnection();
                                    Statement st = con.createStatement();
                                    String messageId = request.getParameter("id");
                                    String query = "SELECT * FROM messages WHERE id = " + messageId;
                                    ResultSet rs = st.executeQuery(query);
                                    if (rs.next()) {
                                        String description = rs.getString("description");
                                        String nomEnvoyeur = rs.getString("nom_envoyeur");
                                        String titre = rs.getString("titre");
                                        String telephone = rs.getString("telephone");
                                        boolean vue = rs.getBoolean("vue");
                                        String createdAt = rs.getString("created_at");
                                %>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Description</th>
                                            <td><%= description %></td>
                                        </tr>
                                        <tr>
                                            <th>Nom de l'envoyeur</th>
                                            <td><%= nomEnvoyeur %></td>
                                        </tr>
                                        <tr>
                                            <th>Titre</th>
                                            <td><%= titre %></td>
                                        </tr>
                                        <tr>
                                            <th>Téléphone</th>
                                            <td><%= telephone %></td>
                                        </tr>
                                        <tr>
                                            <th>Date de création</th>
                                            <td><%= createdAt %></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <% 
                                    if (!vue) {
                                %>
                                <form action="UpdateMessageServlet" method="post">
                                    <input type="hidden" name="messageId" value="<%= messageId %>">
                                    <button type="submit" class="btn btn-primary mb-1 ml-1">Considérer comme vue</button>
                                </form>
                                <% 
                                    }
                                    con.close();
                                } else {
                                    out.println("Message non trouvé.");
                                }
                            } catch (Exception e) {
                                out.println(e.getMessage());
                            }
                            %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ----------------------------FOOTER PAGES------------------------------- -->
<jsp:include page="footer.html" />
<!-- ----------------------------END FOOTER PAGES------------------------------- -->
<!----------------------------- MODAL PAGES------------------------------- -->
<!-- Logout Modal-->
<jsp:include page="modalLogout.html" />
<!-- ---------------------------- END MODAL PAGES------------------------------- -->
<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>
